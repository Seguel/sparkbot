import os
import re
import requests
import json
import subprocess

from django.views.generic import View
from django.http import HttpResponse
from django.http import HttpResponseServerError
from django.conf import settings


class WebhookView(View):

    def post(self, request, *args, **kwargs):
        try:
            json_data = json.loads(request.body)
            resource = json_data['resource']
            wh_id = json_data['data']['id']
            # room_id = json_data['data']['roomId']
            from_person = json_data['data']['personEmail']
        except ValueError:
            return HttpResponseServerError("Invalid webhook data from spark: error1")
        except KeyError:
            return HttpResponseServerError("Invalid webhook data from spark: error2")
        if resource == 'messages':
            headers = {'Authorization': 'Bearer {}'.format(settings.SPARK_ACCESS_TOKEN)}
            try:
                r = requests.get('https://api.ciscospark.com/v1/messages/{}/?mentionedPeople=me'.format(wh_id), headers=headers)
                data = re.match(r"(CookieMonster)( *)(.*)", r.json()['text'])
            except requests.exceptions.RequestException:
                return HttpResponseServerError("Network problem")
            except KeyError:
                return HttpResponseServerError("Invalid json response")
            if data:
                # to_me = data.group(1)
                text = data.group(3)
                if text:
                    if text.rstrip() == "about":
                        message = open(os.path.join(settings.PROJECT_ROOT, "about.txt")).read()
                    elif text.rstrip() == "help":
                        message = open(os.path.join(settings.PROJECT_ROOT, "help.txt")).read()
                    elif text.rstrip() == "howto":
                        message = open(os.path.join(settings.PROJECT_ROOT, "howto.txt")).read()
                    else:
                        message = "mmmm...no entiendo que significa: {}".format(text)
                else:
                    message = "Aqui va una galletita para ti! Nom, nom, nom \n"
                    message += "------\n" + subprocess.Popen(["/usr/games/fortune", "-s"], stdout=subprocess.PIPE).communicate()[0] + "------\n"
                    message += "Nom, nom, nom. Estuvo sabrosa?"
                payload = {
                    # 'roomId': room_id,
                    'toPersonEmail': from_person,
                    'text': message,
                }
                headers = {
                    'Authorization': 'Bearer {}'.format(settings.SPARK_ACCESS_TOKEN),
                    'content-type': 'application/json',
                }
                r = requests.post('https://api.ciscospark.com/v1/messages/',
                                  headers=headers,
                                  data=json.dumps(payload)
                                  )
                print r.json()
        return HttpResponse(status=200)
