from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from .views import WebhookView

urlpatterns = [
    url(r'', csrf_exempt(WebhookView.as_view()), name="webhook"),
]
